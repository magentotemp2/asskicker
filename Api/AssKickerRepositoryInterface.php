<?php


namespace MageTemp\CallForKickingAss\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface AssKickerRepositoryInterface
{

    /**
     * Save AssKicker
     * @param \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface $assKicker
     * @return \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface $assKicker
    );

    /**
     * Retrieve AssKicker
     * @param string $asskickerId
     * @return \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($asskickerId);

    /**
     * Retrieve AssKicker matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageTemp\CallForKickingAss\Api\Data\AssKickerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete AssKicker
     * @param \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface $assKicker
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface $assKicker
    );

    /**
     * Delete AssKicker by ID
     * @param string $asskickerId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($asskickerId);
}
