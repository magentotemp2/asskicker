<?php


namespace MageTemp\CallForKickingAss\Api\Data;

interface AssKickerSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get AssKicker list.
     * @return \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
