<?php


namespace MageTemp\CallForKickingAss\Model;

use MageTemp\CallForKickingAss\Api\AssKickerRepositoryInterface;
use MageTemp\CallForKickingAss\Api\Data\AssKickerSearchResultsInterfaceFactory;
use MageTemp\CallForKickingAss\Api\Data\AssKickerInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use MageTemp\CallForKickingAss\Model\ResourceModel\AssKicker as ResourceAssKicker;
use MageTemp\CallForKickingAss\Model\ResourceModel\AssKicker\CollectionFactory as AssKickerCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class AssKickerRepository implements AssKickerRepositoryInterface
{

    protected $resource;

    protected $assKickerFactory;

    protected $assKickerCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataAssKickerFactory;

    private $storeManager;

    /**
     * @param ResourceAssKicker $resource
     * @param AssKickerFactory $assKickerFactory
     * @param AssKickerInterfaceFactory $dataAssKickerFactory
     * @param AssKickerCollectionFactory $assKickerCollectionFactory
     * @param AssKickerSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceAssKicker $resource,
        AssKickerFactory $assKickerFactory,
        AssKickerInterfaceFactory $dataAssKickerFactory,
        AssKickerCollectionFactory $assKickerCollectionFactory,
        AssKickerSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->assKickerFactory = $assKickerFactory;
        $this->assKickerCollectionFactory = $assKickerCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataAssKickerFactory = $dataAssKickerFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface $assKicker
    ) {
        /* if (empty($assKicker->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $assKicker->setStoreId($storeId);
        } */
        try {
            $this->resource->save($assKicker);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the assKicker: %1',
                $exception->getMessage()
            ));
        }
        return $assKicker;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($assKickerId)
    {
        $assKicker = $this->assKickerFactory->create();
        $this->resource->load($assKicker, $assKickerId);
        if (!$assKicker->getId()) {
            throw new NoSuchEntityException(__('AssKicker with id "%1" does not exist.', $assKickerId));
        }
        return $assKicker;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->assKickerCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface $assKicker
    ) {
        try {
            $this->resource->delete($assKicker);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the AssKicker: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($assKickerId)
    {
        return $this->delete($this->getById($assKickerId));
    }
}
