<?php


namespace MageTemp\CallForKickingAss\Model\ResourceModel;

class AssKicker extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magetemp_callforkickingass_asskicker', 'asskicker_id');
    }
}
