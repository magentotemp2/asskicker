<?php


namespace MageTemp\CallForKickingAss\Model\ResourceModel\AssKicker;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \MageTemp\CallForKickingAss\Model\AssKicker::class,
            \MageTemp\CallForKickingAss\Model\ResourceModel\AssKicker::class
        );
    }
}
