<?php


namespace MageTemp\CallForKickingAss\Model;

use MageTemp\CallForKickingAss\Api\Data\AssKickerInterface;

class AssKicker extends \Magento\Framework\Model\AbstractModel implements AssKickerInterface
{

    protected $_eventPrefix = 'magetemp_callforkickingass_asskicker';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\MageTemp\CallForKickingAss\Model\ResourceModel\AssKicker::class);
    }

    /**
     * Get asskicker_id
     * @return string
     */
    public function getAsskickerId()
    {
        return $this->getData(self::ASSKICKER_ID);
    }

    /**
     * Set asskicker_id
     * @param string $asskickerId
     * @return \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface
     */
    public function setAsskickerId($asskickerId)
    {
        return $this->setData(self::ASSKICKER_ID, $asskickerId);
    }

    /**
     * Get id
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return \MageTemp\CallForKickingAss\Api\Data\AssKickerInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }
}
