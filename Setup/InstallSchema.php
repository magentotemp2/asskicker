<?php


namespace MageTemp\CallForKickingAss\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_magetemp_callforkickingass_asskicker = $setup->getConnection()->newTable($setup->getTable('magetemp_callforkickingass_asskicker'));

        $table_magetemp_callforkickingass_asskicker->addColumn(
            'asskicker_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_magetemp_callforkickingass_asskicker->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'id'
        );

        $setup->getConnection()->createTable($table_magetemp_callforkickingass_asskicker);
    }
}
